#!/usr/bin/env python3
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotInteractableException
from selenium.common.exceptions import StaleElementReferenceException

import sys
import datetime

def main():
    if (len(sys.argv) != 3):
        print("usage: " + sys.argv[0] + " <username> <password>")
        sys.exit()
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(options=chrome_options)
    driver.get("https://www.ixl.com/math/calculus/power-rule")
    username = sys.argv[1]
    pword = sys.argv[2]
    login(username, pword, driver)
    score = 0
    print("Logged in")
    sleep(4)
    try:
        while (True):
            try:
            # Negative Logic
                tmpstr = driver.find_element_by_class_name("number").text
                if tmpstr == '':
                    if score == 99:
                        driver.refresh()
                    sleep(0.5)
                    continue

                try:
                    driver.find_element_by_class_name("negSign") 

                except NoSuchElementException:
                    number = int(tmpstr)
                    negative = False

                else:
                    number = -1 * int(tmpstr)
                    negative = True

                # Fraction Logic
                try:
                    driver.find_element_by_class_name("numerator").find_element_by_class_name("number")

                except NoSuchElementException:
                    is_fraction = False

                else:
                    is_fraction = True

                if is_fraction:
                    answer = fraction(negative, driver)
                else:
                    answer = str(number) + "x^" + str(number-1)

                answer_box = driver.find_element_by_class_name("proxy-input")
                answer_box.send_keys(answer)
                answer_box.send_keys(Keys.RETURN)

                sleep(1)
                score = int(driver.find_element_by_id("currentscore").text)
                print(str(score) + "\t" + str(datetime.datetime.now()))
                if (score == 91):
                    sleep(4)
                if (score == 100) or (score == 97):
                    driver.get("https://www.ixl.com/math/calculus/power-rule")
            except ElementNotInteractableException as ex:
                # print(repr(ex), file=sys.stderr)
                sleep(1)
                continue

            # except StaleElementReferenceException as ex:
            #     print(repr(ex), file=sys.stderr)
            #     driver.refresh()
            #     sleep(1)
            #     continue

            except ValueError as ex:
                # print(ex)
                sleep(1)
                continue


    except Exception as ex:
        print(repr(ex), file=sys.stderr)
        driver.close()

def login(uname, pword, driver):
    sleep(1)

    close_popup = driver.find_element_by_class_name("explore-btn")
    close_popup.click()

    username_slot = driver.find_element_by_id("qlusername")
    username_slot.clear()
    username_slot.send_keys(uname)

    password_slot = driver.find_element_by_id("qlpassword")
    password_slot.clear()
    password_slot.send_keys(pword)

    login_button = driver.find_element_by_id("qlsubmit")
    login_button.click()

def fraction(negative, driver):
    numerator = int(driver.find_element_by_class_name("numerator").find_element_by_class_name("number").text)
    denominator = int(driver.find_element_by_class_name("denominator").find_element_by_class_name("number").text)

    if negative:
        return str("-" + str(numerator) + "/" + str(denominator) + "x^-" + str(numerator + denominator) + "/" + str(denominator))
    else: 
        return str(str(numerator) + "/" + str(denominator) + "x^" + str(numerator - denominator) + "/" + str(denominator))

main()
